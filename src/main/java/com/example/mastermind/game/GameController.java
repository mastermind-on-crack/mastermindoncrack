package com.example.mastermind.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class GameController {

    private GameService gameService;

    @Autowired
    public GameController(GameService gameService ) {
        this.gameService = gameService;

    }

    //TODO fix URLS

    @GetMapping(path="get/mastercode")
    public List<Color> getCode(){
        return gameService.getSecretCode();
    }

    @GetMapping(path="/start/mastermind")
    public void gameInitialization(){
        gameService.setSecretCode();
    }

    @PostMapping(path="/play/mastermind")
    public GameDetails getClues(@RequestBody List<Color> guess){
        return gameService.playRound(guess);
    }

    @RequestMapping(path ="/get/gamedetails")
    public GameDetails getGameDetails(){
        return gameService.getGameDetails();
    }
}
