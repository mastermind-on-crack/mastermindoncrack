package com.example.mastermind.game;

public enum Color {

    RED(0),
    BLUE(1),
    YELLOW(2),
    GREEN(3),
    WHITE(4),
    BLACK(5);

    private int id;

    Color(int id) {
        this.id = id;
    }

    public static Color valueOfId(int id) {
        for (Color e : values()) {
            if (e.id == id) {
                return e;
            }
        }
        return null;
    }
}
