package com.example.mastermind.game;

public enum GameState {

    INITIATE,
    RUNNING,
    GAMEOVER,
    EXIT;
}
