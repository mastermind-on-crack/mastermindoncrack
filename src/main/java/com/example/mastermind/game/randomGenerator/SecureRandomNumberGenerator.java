package com.example.mastermind.game.randomGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;


@Getter
@NoArgsConstructor
@Service
public class SecureRandomNumberGenerator {

    private SecureRandom sc = new SecureRandom();
    private int []rndInt;


    public int[] generateRandomNumber(int length,int numberOfColors){
        this.rndInt = new int[length];
        for(int i=0;i<4;i++){
            this.rndInt[i]= sc.nextInt(numberOfColors);
        }
        return rndInt;
    }
}
