package com.example.mastermind.game;

public enum ScoreState {
    WON,
    LOST,
    PENDING;
}
