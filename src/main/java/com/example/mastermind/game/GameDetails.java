package com.example.mastermind.game;


import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;


import java.util.List;


@Getter
@Setter
@Service
public class GameDetails {
    private final int maxRound = 10;
    private final int maxScore = 4;
    private GameState gameState;
    private ScoreState scoreState;
    private List<Color> secretCode;
    private List<Clues> clues;
    private int round;
    private int score;

    public GameDetails(){
        this.gameState =  GameState.INITIATE;
        this.scoreState = ScoreState.PENDING;
        this.round = 1;
    }

    public void incrementScore(){
        this.score ++;
    }

    public void incrementRound(){
        this.round ++;
    }
}
