package com.example.mastermind.game;

import com.example.mastermind.game.randomGenerator.SecureRandomNumberGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class GameService  {

    private SecureRandomNumberGenerator secureRandomNumberGenerator;
    private GameDetails gameDetails;

    @Autowired
    public GameService(SecureRandomNumberGenerator secureRandomNumberGenerator, GameDetails gameDetails) {
        this.secureRandomNumberGenerator = secureRandomNumberGenerator;
        this.gameDetails = gameDetails;
    }

    public List<Color> getSecretCode() {
        return gameDetails.getSecretCode();
    }

    public GameDetails getGameDetails() {
        return gameDetails;
    }

    public void setSecretCode(){
        gameDetails.setSecretCode(createSecretCode());
        System.out.println(gameDetails.getSecretCode());
    }

    public List<Color> createSecretCode( ) {
        int[] rndNumbers = secureRandomNumberGenerator.generateRandomNumber(gameDetails.getMaxScore(), Color.values().length);
        List<Color> createSecretCode = new ArrayList<Color>();

        for(int i=0;i<rndNumbers.length;i++){
            createSecretCode.add(Color.valueOfId(rndNumbers[i]));
        }
        return createSecretCode;
    }

    public GameDetails playRound(List<Color> currentGuess) {
        gameDetails.setScore(0);
        gameDetails.setClues(getClues(currentGuess));

        if(isGameOver()==true){
            gameDetails.setGameState(GameState.GAMEOVER);
            if(gameDetails.getScore()== gameDetails.getMaxScore() || gameDetails.getRound()== gameDetails.getMaxRound()){
                gameDetails.setScoreState(ScoreState.WON);
            }else{
                gameDetails.setScoreState(ScoreState.LOST);
            }
            System.out.println("GAME IS OVER, YOU "+ gameDetails.getScoreState()+ "!");
            gameDetails.setGameState(GameState.GAMEOVER);
        }
        gameDetails.incrementRound();
        //System.out.println(clues);
        return gameDetails;
    }

    //todo
    public List<Clues> getClues(List<Color> guess){
        gameDetails.setGameState(GameState.RUNNING);
        List<Clues> clues = new ArrayList<>();
        List<Boolean> matched = Arrays.asList(false, false, false, false);
        List<Boolean> used = Arrays.asList(false, false, false, false);

        System.out.println(guess);

        for(int i=0;i<guess.size();i++){
            if(guess.get(i)== gameDetails.getSecretCode().get(i)){
                clues.add(Clues.BLACK);
                matched.set(i, true);
                used.set(i, true);
                gameDetails.incrementScore();
            }
        }

        for(int i =0;i<guess.size();i++){
            if (matched.get(i)) {
                continue;
            }
            for(int j = 0; j< gameDetails.getSecretCode().size(); j++){
                if(i!=j && guess.get(i) == gameDetails.getSecretCode().get(j) && used.get(j)==false){
                    clues.add(Clues.WHITE);
                    used.set(j, true);
                    break;
                }
            }
        }
        System.out.println(clues);
        return clues;
    }

    public boolean isGameOver(){
        if(gameDetails.getScore()== gameDetails.getMaxScore() || gameDetails.getRound()== gameDetails.getMaxRound()){
            return true;
        }
        return false;
    }
}
